import geolocation.*
import mission.RoverStatus
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import testmaterial.DisplayFake
import testmaterial.grid


val Mars = """
    ┌─────────────────────────────┐
    │                       #     │
    │                       #     │
    │   #    #     1              │
    │                   #         │
    │                             │
    │    ##    ##  3          #   │
    │    ##             #         │  1. Jezero
    │                             │  2. Aeolis Pallus
    │           #       #    #   2│  3. Meridian Plains
    └─────────────────────────────┘
""".grid().planet()

class AcceptanceTest {
    val displayFake = DisplayFake()

    @Test
    fun `You are given the initial starting point (x,y) of a rover and the direction (N,S,E,W) it is facing`() {
        val mission = land(
            `the rover` = "Curiosity",
            `on the planet` = Mars,
            `at site` = "Jezero".location,
            facing = Compas.South,
            `transmitting report to` = displayFake
        )

        val report = mission.transmit("")
        assertThat(report.position).isEqualTo("Jezero".location)
        assertThat(report.orientation).isEqualTo(Compas.South)
        assertThat(report.status).isEqualTo(RoverStatus.HAVCO)
    }

    @Test
    fun `Implement commands that move the rover forward (f)`() {
        val mission = land(
            `the rover` = "Curiosity",
            `on the planet` = Mars,
            `at site` = "Jezero".location,
            facing = Compas.South,
            `transmitting report to` = displayFake
        )

        val report =  mission.transmit("ff")
        assertThat(report.position).isEqualTo("Jezero".location.south(2.meters))
        assertThat(report.orientation).isEqualTo(Compas.South)
        assertThat(report.status).isEqualTo(RoverStatus.HAVCO)
    }

    @Test
    fun `Implement commands that move the rover backward (b)`() {
        val mission = land(
            `the rover` = "Curiosity",
            `on the planet` = Mars,
            `at site` = "Jezero".location,
            facing = Compas.South,
            `transmitting report to` = displayFake
        )

        val report = mission.transmit("bb")

        assertThat(report.position).isEqualTo("Jezero".location.north(2.meters))
        assertThat(report.orientation).isEqualTo(Compas.South)
        assertThat(report.status).isEqualTo(RoverStatus.HAVCO)
    }

    @Test
    fun `Implement commands that turn the rover left (l)`() {
        val mission = land(
            `the rover` = "Curiosity",
            `on the planet` = Mars,
            `at site` = "Jezero".location,
            facing = Compas.South,
            `transmitting report to` = displayFake
        )

        val report = mission.transmit("l")

        assertThat(report.position).isEqualTo("Jezero".location)
        assertThat(report.orientation).isEqualTo(Compas.East)
        assertThat(report.status).isEqualTo(RoverStatus.HAVCO)
    }

    @Test
    fun `Implement commands that turn the rover right (r)`() {
        val mission = land(
            `the rover` = "Curiosity",
            `on the planet` = Mars,
            `at site` = "Jezero".location,
            facing = Compas.South,
            `transmitting report to` = displayFake
        )

        val report = mission.transmit("r")

        assertThat(report.position).isEqualTo("Jezero".location)
        assertThat(report.orientation).isEqualTo(Compas.West)
        assertThat(report.status).isEqualTo(RoverStatus.HAVCO)
    }

    @Test
    fun `Implement wrapping at edges But be careful, planets are spheres`() {
        val mission = land(
            `the rover` = "Curiosity",
            `on the planet` = Mars,
            `at site` = "Aeolis Pallus".location,
            facing = Compas.East,
            `transmitting report to` = displayFake
        )

        val report = mission.transmit("f")

        assertThat(report.position).isEqualTo(Position(0, 0))
        assertThat(report.orientation).isEqualTo(Compas.East)
        assertThat(report.status).isEqualTo(RoverStatus.HAVCO)
    }

    @Test
    fun `Implement obstacle detection before each move to a new square`() {
        val mission = land(
            `the rover` = "Curiosity",
            `on the planet` = Mars,
            `at site` = "Meridian Plains".location,
            facing = Compas.West,
            `transmitting report to` = displayFake
        )

        val report = mission.transmit("ffff")

        assertThat(report.position).isEqualTo("Meridian Plains".location.west(2.meters))
        assertThat(report.orientation).isEqualTo(Compas.West)
        assertThat(report.status).isEqualTo(RoverStatus.WONTCO)
    }
}



private val String.location: Position
    get() = Mars.`position of`(this)
