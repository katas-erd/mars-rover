package geolocation

import mission.RoverStatus
import testmaterial.grid
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class NavigationSystemShould {

    @Test
    fun `land at the given position`() {
        `given a planet of size`( 4, 5)
        .`when landing at`(Position(2, 3))
        .`then it should be at`(Position(2, 3))
    }

    @Test
    fun `go to the given position`() {
        `given a planet of size`( 10, 10)
            .`when landing at`(Position(2, 1))
            .`when going to`(Compas.North, 2.meters)
            .`then it should be at`(Position(2, 3))
    }

    @Test
    fun `bound to the edges`() {
        `given a planet of size`( 10, 10)
            .`when landing at`(Position(2, 9))
            .`when going to`(Compas.North, 1.meters)
            .`then it should be at`(Position(2, 0))
    }

    @Test
    fun `detect obstacles`() {
        `when landing on the planet`("""
                        ┌─────┐
                        │ #   │
                        │ X   │
                        │     │
                        └─────┘
                        """
        ).`then the rover cannot go`(Compas.North)
        .`then the rover can go`(Compas.South)
        .`then the rover can go`(Compas.East)
        .`then the rover can go`(Compas.West)
    }

    private fun `given a planet of size`(width: Int, height: Int) = Size(width, height)
    private fun Size.`when landing at`(position: Position) =
        NavigationSystem(position, Planet(this, emptyList(), emptyList()))

    private fun `when landing on the planet`(description: String) = description.grid().coordinateSystem()
}

private fun NavigationSystem.`then the rover cannot go`(orientation: Compas): NavigationSystem {
    assertThat(this.`go to`(orientation, 1.meters)).isEqualTo(RoverStatus.WONTCO)
    return this
}

private fun NavigationSystem.`then the rover can go`(orientation: Compas): NavigationSystem {
    assertThat(this.`go to`(orientation, 1.meters)).isEqualTo(RoverStatus.HAVCO)
    return this
}

private fun NavigationSystem.`when going to`(orientation: Compas, distance: Distance): NavigationSystem {
    this.`go to`(orientation, distance)
    return this
}

private fun NavigationSystem.`then it should be at`(expected: Position) {
    assertThat(this.position).isEqualTo(expected)
}