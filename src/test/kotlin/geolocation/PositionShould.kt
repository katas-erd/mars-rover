package geolocation

import testmaterial.grid
import testmaterial.of
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import testmaterial.position


class PositionShould {
    @Test
    fun `give the position to its north`() {
        Compas.North of """
                            ┌─────┐
                            │     │
                            │  X  │
                            └─────┘
            """ `should be` """
                            ┌─────┐
                            │  X  │
                            │     │
                            └─────┘
                            """
    }


    @Test
    fun `give the position to its south`() {
        Compas.South of """
                            ┌─────┐
                            │  X  │
                            │     │
                            └─────┘
            """ `should be` """
                            ┌─────┐
                            │     │
                            │  X  │
                            └─────┘
                            """
    }

    @Test
    fun `give the position to its east`() {
        Compas.East of """
                            ┌─────┐
                            │     │
                            │  X  │
                            └─────┘
            """ `should be` """
                            ┌─────┐
                            │     │
                            │   X │
                            └─────┘
                            """
    }

    @Test
    fun `give the position to its west`() {
        Compas.West of """
                            ┌─────┐
                            │     │
                            │  X  │
                            └─────┘
            """ `should be` """
                            ┌─────┐
                            │     │
                            │ X   │
                            └─────┘
                            """
    }
}
infix fun Position.`should be`(expected: String) = assertThat(this).isEqualTo(expected.grid().position())