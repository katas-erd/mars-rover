package geolocation

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class OrientationShould {

    @Test
    fun `define the left of each position`() {
        `turning left when we are facing`(Compas.North)
            .`then whe should face`(Compas.West)
            .`then whe should face`(Compas.South)
            .`then whe should face`(Compas.East)
            .`then whe should face`(Compas.North)
    }

    @Test
    fun `define the right of each position`() {
        `turning right when we are facing`(Compas.North)
            .`then whe should face`(Compas.East)
            .`then whe should face`(Compas.South)
            .`then whe should face`(Compas.West)
            .`then whe should face`(Compas.North)
    }

    class P(val orientation: Compas, val action: (Compas) -> Compas) {
        fun `then whe should face`(next: Compas): P {
            assertThat(action(orientation)).isEqualTo(next)
            return P(next, action)
        }
    }
    private fun `turning left when we are facing`(initial: Compas) = P(initial) { orientation -> orientation.left }
    private fun `turning right when we are facing`(initial: Compas) = P(initial) { orientation -> orientation.right }


}

