package testmaterial

import geolocation.*
import mission.Rover

data class Item(val symbol: Char, val position: Position)

data class Legend(val text: String, val number: Int)
data class Grid(val size: Size, val items: List<Item>, val legends: List<Legend>) {
    fun obstacles() = items.filter { it.symbol == '#' }.map { Obstacle(it.position) }

    fun rover() : Rover {
        val rover = items.first { listOf('^', '<', '>', 'v').contains(it.symbol) }
        return Rover(NavigationSystem(rover.position, Planet(size, this.obstacles(), this.locations())), rover.symbol.toOrientation())
    }

    fun locations() = legends.map { point ->
        Location(point.text, items.first { it.symbol == point.number.toString().first() }.position)
    }
    fun coordinateSystem() = NavigationSystem(`landing at` = this.position(), planet())

    fun planet() = Planet(size, obstacles(), locations())
}



fun String.parse(): List<Rover> = this.grids().map { it.rover() }

fun String.grid() = this.trimIndent().split("\n").map { it.trim() }.reversed().grid()


fun List<String>.legends(): List<Legend> {
    val rx = ".*(\\d)\\. (.+)".toRegex()
    return this.mapNotNull {
        val group = rx.find(it)
        if (group != null) {
            Legend(group.groupValues[2], group.groupValues[1].toInt())
        } else {
            null
        }
    }
}
fun List<String>.grid(): Grid {

    val cases = this.drop(1).dropLast(1).map {
        val start = it.indexOf("│")
        val end = it.indexOf("│", start + 1)
        it.substring(start + 1, end)
    }

    val width = cases.first().count()
    val height = cases.count()

    val items = cases.mapIndexed { y, s -> s.mapIndexed { x, c -> Item(c, Position(x, y)) } }.flatten().filter { it.symbol != ' ' }

    return Grid(Size(width, height), items, this.legends())
}

fun String.grids(): List<Grid> {
    val lines = trimIndent().split("\n")

    var columnFirstActual = lines.first().indexOf('┌')
    val grids = mutableListOf<List<String>>()
    while( columnFirstActual != -1) {
        val columnLastActual = lines.first().indexOf('┐', columnFirstActual)
        grids.add(
            lines.map { line -> line.subSequence(columnFirstActual, columnLastActual + 1).toString() }.reversed()
        )

        columnFirstActual = lines.first().indexOf('┌', columnFirstActual + 1)
    }

    return grids.map { it.grid() }
}

fun Grid.position() = items.first { it.symbol == 'X' }.position
infix fun Compas.of(initial: String): Position =
    when(this) {
        Compas.North -> initial.grid().position().north(1.meters)
        Compas.South -> initial.grid().position().south(1.meters)
        Compas.East -> initial.grid().position().east(1.meters)
        Compas.West -> initial.grid().position().west(1.meters)
    }

fun Char.toOrientation() = when(this) {
    '^' -> Compas.North
    'v' -> Compas.South
    '<' -> Compas.West
    '>' -> Compas.East
    else -> Compas.North
}



/*
⬉ ⬈ ⬊ ⬋⬅➡ ⬆ ⬇
*/
