package testmaterial

import geolocation.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class HelperTest {
    @Test
    fun `should create a grid`() {
        val grid = """
        ┌──┐
        │  │
        │# │
        │^.│
        └──┘ 
        """.grid()

        assertThat(grid.items).containsExactlyInAnyOrder(
            Item( '^', Position(0, 0)),
            Item( '#', Position(0, 1)),
            Item( '.', Position(1, 0))
        )
        assertThat(grid.size).isEqualTo(Size(2, 3))
    }

    @Test
    fun `should create a grids`() {
        val grid = """
        ┌──┐ ┌──┐
        │# │ │ ;│
        │^.│ │  │
        └──┘ └──┘ 
        """.grids()

        assertThat(grid.count()).isEqualTo(2)
        assertThat(grid.first().items).containsExactlyInAnyOrder(
            Item( '^', Position(0, 0)),
            Item( '#', Position(0, 1)),
            Item( '.', Position(1, 0))
        )
        assertThat(grid.last().items).containsExactlyInAnyOrder(
            Item( ';', Position(1, 1))
        )
    }

    @Test
    fun `should find a rover`() {
        val orientations = listOf(Compas.East, Compas.North, Compas.West, Compas.South)
        """
        ┌──┐ ┌──┐ ┌──┐ ┌──┐
        │  │ │  │ │  │ │  │
        │> │ │^ │ │< │ │v │
        │  │ │  │ │  │ │  │
        └──┘ └──┘ └──┘ └──┘ 
        """.grids().forEachIndexed {i, grid ->
            assertThat(grid.rover().position()).isEqualTo(Position(0, 1))
            assertThat(grid.rover().orientation()).isEqualTo(orientations[i])
        }
    }

    @Test
    fun `should find obstacles`() {
        val  grid =
        """
        ┌──┐
        │  │
        │# │
        │  │
        └──┘ 
        """.grid()

        assertThat(grid.obstacles()).containsExactlyInAnyOrder(Obstacle(Position(0, 1)))
    }

    @Test
    fun `should find location`() {
        val  grid =
            """
        ┌──┐
        │  │
        │1 │
        │  │ 1. Point A
        └──┘ 
        """.grid()

        assertThat(grid.locations()).containsExactlyInAnyOrder(Location("Point A", Position(0, 1)))
    }
}