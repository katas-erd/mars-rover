package testmaterial

import mission.Monitor
import mission.Rover
import mission.RoverStatus

class DisplayFake: Monitor {
    lateinit var status: RoverStatus
    override fun display(rover: Rover, status: RoverStatus) {
        this.status = status
    }

}