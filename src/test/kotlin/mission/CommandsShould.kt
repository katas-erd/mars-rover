package mission

import testmaterial.Grid
import testmaterial.grids
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import testmaterial.grid


private typealias Action = () -> RoverStatus
class CommandsShould {

    @Test
    fun `move forward`() {
        `given the following situation`(
        """
        ┌─────┐
        │     │
        │ ^   │
        └─────┘ 
        """
        ).`when we send the command`("f")
        .`then we should see`(
        """
        ┌─────┐
        │ ^   │
        │     │
        └─────┘ 
        """
        )
    }

    @Test
    fun `move backward`() {
        `given the following situation`(
            """
        ┌─────┐
        │ ^   │
        │     │
        └─────┘ 
        """
        ).`when we send the command`("b")
            .`then we should see`(
                """
        ┌─────┐
        │     │
        │ ^   │
        └─────┘ 
        """
            )
    }

    @Test
    fun `turn left`() {
        `given the following situation`(
            """
        ┌─────┐
        │ ^   │
        │     │
        └─────┘ 
        """
        ).`when we send the command`("l")
            .`then we should see`(
                """
        ┌─────┐
        │ <   │
        │     │
        └─────┘ 
        """
            )
    }

    @Test
    fun `turn right`() {
        `given the following situation`(
            """
        ┌─────┐
        │ ^   │
        │     │
        └─────┘ 
        """
        ).`when we send the command`("r")
            .`then we should see`(
"""
        ┌─────┐
        │ >   │
        │     │
        └─────┘ 
        """
            )
    }

    @Test
    fun `chain commands`() {
        `given the following situation`(
            """
        ┌─────┐
        │ ^   │
        │     │
        └─────┘ 
        """
        ).`when we send the command`("rffrflb")
            .`then we should see`(
                """
        ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ 
        │>    │ │  >  │ │   > │ │   v │ │     │ │     │ │     │ 
        │     │ │     │ │     │ │     │ │   v │ │   > │ │  >  │ 
        └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘  
        """
            )
    }

    @Test
    fun `deal with obstacles`() {
        `given the following situation`(
            """
        ┌─────┐
        │ ^   │
        │     │
        └─────┘ 
        """
        ).`when we send the command`("rffrflb")
            .`then we should see`(
                """
        ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ ┌─────┐ 
        │>    │ │  >  │ │   > │ │   v │ │     │ │     │ │     │ 
        │     │ │     │ │     │ │     │ │   v │ │   > │ │  >  │ 
        └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘ └─────┘  
        """
            )
    }

    @Test
    fun `reject wrong command`() {
        `given the following situation`(
            """
        ┌─────┐
        │ ^   │
        │     │
        └─────┘ 
        """
        ).`when we send the command`("Z")
        .`then we should get an error`()
    }

    @Test
    fun `abort when encountering an obstacle`() {
        `given the following situation`(
            """
        ┌─────┐
        │ ^   │
        │     │
        └─────┘ 
        """)
    }
}

private fun Pair<Rover, Action>.`then we should get an error`() {
    val (_, action) = this
    assertThat(action()).isNotEqualTo(RoverStatus.HAVCO)
}
private fun Pair<Rover, Action>.`then we should see`(expectedGrid: String) {
    val (rover, action) = this

    action()

    val expected = expectedGrid.trimIndent().grids().last().rover()

    assertThat(rover.position())
        .isEqualTo(expected.position())
    assertThat(rover.orientation())
        .isEqualTo(expected.orientation())
}

private fun Grid.`when we send the command`(commands: String): Pair<Rover, Action> {
    val rover = this.rover()
    val controlPanel = ControlPanel(piloting = rover)
    return rover to { controlPanel.send(commands) }
}


fun `given the following situation`(grid: String) = grid.trimIndent().grid()