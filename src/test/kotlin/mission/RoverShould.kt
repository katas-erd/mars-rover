package mission

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import testmaterial.parse

private typealias RoverAction = Rover.() -> RoverStatus
class RoverShould {

    @Test
    fun `move forward`() = Rover::`move forward`.`should move the rover in the right direction`(
        """
        ┌─────┐    ┌─────┐
        │     │    │ ^   │
        │ ^   │    │     │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │ v   │    │     │
        │     │    │ v   │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │     │    │     │
        │>    │    │ >   │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │     │    │     │
        │    <│    │   < │
        └─────┘    └─────┘
        """
    )

    @Test
    fun `move backward`() = Rover::`move backward`.`should move the rover in the right direction`(
        """
        ┌─────┐    ┌─────┐
        │ ^   │    │     │
        │     │    │ ^   │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │     │    │ v   │
        │ v   │    │     │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │     │    │     │
        │ >   │    │>    │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │     │    │     │
        │   < │    │    <│
        └─────┘    └─────┘
        """
    )

    @Test
    fun `turn left`() = Rover::`turn left`.`should turn the rover in the right orientation`(
        """
        ┌─────┐    ┌─────┐
        │ ^   │    │ <   │
        │     │    │     │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │ <   │    │ v   │
        │     │    │     │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │ v   │    │ >   │
        │     │    │     │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │ >   │    │ ^   │
        │     │    │     │
        └─────┘    └─────┘
        """
    )

    @Test
    fun `turn right`() = Rover::`turn right`.`should turn the rover in the right orientation`(
        """
        ┌─────┐    ┌─────┐
        │ ^   │    │ >   │
        │     │    │     │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │ >   │    │ v   │
        │     │    │     │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │ v   │    │ <   │
        │     │    │     │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │ <   │    │ ^   │
        │     │    │     │
        └─────┘    └─────┘
        """
    )

    @Test
    fun `move around the planet`() = Rover::`move forward`.`should move the rover in the right direction`(
        """
        ┌─────┐    ┌─────┐
        │  ^  │    │     │
        │     │    │     │
        │     │    │     │
        │     │    │  ^  │
        └─────┘    └─────┘
        """,
        """
        ┌─────┐    ┌─────┐
        │     │    │  v  │
        │     │    │     │
        │     │    │     │
        │  v  │    │     │
        └─────┘    └─────┘
        """,
        """
        ┌───────┐    ┌───────┐
        │      >│    │>      │
        │       │    │       │
        └───────┘    └───────┘
        """,
        """
        ┌───────┐    ┌───────┐
        │<      │    │      <│
        │       │    │       │
        └───────┘    └───────┘
        """
    )

    @Test
    fun `should stop in front of an obstacle`() = Rover::`move forward`.`should move the rover in the right direction`(
        """
        ┌─────┐ ┌─────┐ ┌─────┐
        │  v  │ │     │ │     │
        │     │ │  v  │ │  v  │
        │  #  │ │  #  │ │  #  │
        │     │ │     │ │     │
        └─────┘ └─────┘ └─────┘
        """,
        """
        ┌───────┐ ┌───────┐ ┌───────┐
        │> #    │ │ >#    │ │ >#    │
        │       │ │       │ │       │
        └───────┘ └───────┘ └───────┘
        """,
        """
        ┌───────┐ ┌───────┐ ┌───────┐
        │  # <  │ │  #<   │ │  #<   │
        │       │ │       │ │       │
        └───────┘ └───────┘ └───────┘
        """,
        """
        ┌─────┐ ┌─────┐ ┌─────┐
        │  #  │ │  #  │ │  #  │
        │     │ │  ^  │ │  ^  │
        │  ^  │ │     │ │     │
        │     │ │     │ │     │
        └─────┘ └─────┘ └─────┘
        """,
    )

    @Test
    fun `should stop in front of an obstacle when going around the planet`() = Rover::`move forward`.`should move the rover in the right direction`(
        """
        ┌─────┐ ┌─────┐ ┌─────┐
        │  #  │ │  #  │ │  #  │
        │     │ │     │ │     │
        │  v  │ │     │ │     │
        │     │ │  v  │ │  v  │
        └─────┘ └─────┘ └─────┘
        """,
        """
        ┌───────┐ ┌───────┐ ┌───────┐
        │#    > │ │#     >│ │#     >│
        │       │ │       │ │       │
        └───────┘ └───────┘ └───────┘
        """,
        """
        ┌───────┐ ┌───────┐ ┌───────┐
        │ <    #│ │<     #│ │<     #│
        │       │ │       │ │       │
        └───────┘ └───────┘ └───────┘
        """,
        """
        ┌─────┐ ┌─────┐ ┌─────┐
        │     │ │  ^  │ │  ^  │
        │  ^  │ │     │ │     │
        │     │ │     │ │     │
        │  #  │ │  #  │ │  #  │
        └─────┘ └─────┘ └─────┘
        """,
    )
}

private fun RoverAction.`should turn the rover in the right orientation`(vararg s: String) =
    `should move the rover in the right direction`(*s)
private fun RoverAction.`should move the rover in the right direction`(vararg s: String) {
    val f = this
    s.map {
        it.trimIndent().parse()
    }.forEach { steps ->
        var previous = steps.first()
        previous.f()

        steps.drop(1).forEach {expected ->
            assertThat(previous.position())
                .withFailMessage("Rover facing ${previous.orientation()} should be at ${expected.position()} but was ${previous.position()}")
                .isEqualTo(expected.position())
            assertThat(previous.orientation())
                .withFailMessage("Rover facing ${previous.orientation()} and starting at ${previous.position()} should be facing ${expected.orientation()}")
                .isEqualTo(expected.orientation())

            previous = expected
            previous.f()
        }
    }
}
