package geolocation

data class Offset(val dx: Int, val dy: Int) {
    constructor(dx: Distance, dy: Distance): this(dx.value, dy.value)
}