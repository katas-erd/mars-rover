package geolocation

import kotlin.math.abs

data class Size(val width: Int, val height: Int) {
    fun bound(position: Position): Position {
        val (x, y) = position
        return Position(x.bounded(width), y.bounded(height))
    }
    
    private fun Int.bounded(limit: Int): Int {
        if (this < 0) {
            return limit - (abs(this) % limit)
        }
        return this % limit
    }
}