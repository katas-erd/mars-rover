package geolocation

enum class Compas(private val `its left`: () -> Compas, private val `its right`: () -> Compas, val to: (d: Distance) -> Offset) {
    North(`its left` = { West }, `its right` = { East }, to = { d -> Offset(0.meters, d)}),
    South(`its left` = { East }, `its right` = { West }, to = { d -> Offset(0.meters, -d)}),
    East(`its left` = { North }, `its right` = { South }, to = { d -> Offset(d, 0.meters)}),
    West(`its left` = { South }, `its right` = { North }, to = { d -> Offset(-d, 0.meters)});

    val left: Compas
        get() = `its left`()

    val right: Compas
        get() = `its right`()
}