package geolocation

data class Position(val x: Int, val y: Int) {
    operator fun plus(delta: Offset) = Position(x + delta.dx, y + delta.dy)

    fun north(distance: Distance) = move(this, Offset(0, distance.value))
    fun south(distance: Distance) = move(this, Offset(0, -distance.value))
    fun east(distance: Distance) = move(this, Offset(distance.value, 0))
    fun west(distance: Distance) = move(this, Offset(-distance.value, 0))

    private val move: (Position, Offset) -> Position = { p, o -> p + o }
}
