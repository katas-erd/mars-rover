package geolocation

data class Obstacle(val position: Position)
data class Location(val name: String, val position: Position)

class Planet(val size: Size, val obstacles: List<Obstacle>, private val locations: List<Location>) {
    fun `real position from`(position: Position) = size.bound(position)
    fun `has an obstacle at`(position: Position): Boolean = obstacles.firstOrNull { it.position == position } != null
    fun `position of`(name: String) = locations.first { it.name.equals(name, ignoreCase = true) }.position
}