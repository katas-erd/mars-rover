package geolocation

import mission.RoverStatus

class NavigationSystem(`landing at`: Position, on: Planet) {
    private val planet: Planet = on
    private var currentPosition = `landing at`

    val position: Position
        get() = currentPosition

    fun `go to`(orientation: Compas, distance: Distance): RoverStatus {
        val nextPosition = planet.`real position from`(
            position + orientation.to(distance)
        )

        if (nextPosition.`has something blocking the way`()) {
            return RoverStatus.WONTCO
        }
        currentPosition = nextPosition
        return RoverStatus.HAVCO
    }

    private fun Position.`has something blocking the way`() = planet.`has an obstacle at`(this)
}