package geolocation

data class Distance(val value: Int) {
    operator fun unaryMinus() = Distance(-value)
}

val Int.meters: Distance
    get() = Distance(this)