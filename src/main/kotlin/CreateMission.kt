import geolocation.NavigationSystem
import geolocation.Compas
import geolocation.Planet
import geolocation.Position
import mission.ControlPanel
import mission.Mission
import mission.Monitor
import mission.Rover

fun land(
    `the rover`: String,
    `on the planet`: Planet,
    `at site`: Position,
    facing: Compas,
    `transmitting report to`: Monitor
): Mission {
    val navigationSystem = NavigationSystem(`landing at` = `at site`, on = `on the planet`)
    val rover = Rover(
        `guided with` = navigationSystem,
        facing = facing
    )
    val control = ControlPanel(
        piloting = rover
    )

    return Mission(`the rover`, control, `transmitting report to`)
}