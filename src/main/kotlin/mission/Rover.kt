package mission

import geolocation.*

class Rover(`guided with`: NavigationSystem, facing: Compas) {

    private var compas = facing
    private val navigation: NavigationSystem = `guided with`

    fun position() = navigation.position
    fun orientation() = compas

    fun `move forward`() = navigation.`go to`(compas,1.meters)
    fun `move backward`() = navigation.`go to`(compas, -1.meters)

    fun `turn left`(): RoverStatus {
        compas = compas.left
        return RoverStatus.HAVCO
    }
    fun `turn right`(): RoverStatus {
        compas = compas.right
        return RoverStatus.HAVCO
    }
}