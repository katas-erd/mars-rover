package mission

class Mission(private val name: String, private val control: ControlPanel, private val monitor: Monitor) {

    fun transmit(commands: String): Report {
        val status = control.send(commands)
        monitor.display(control.rover, status)
        return report(status)
    }


    private fun report(status: RoverStatus) = Report(control.rover.position(), control.rover.orientation(), status)
}