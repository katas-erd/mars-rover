package mission

import geolocation.Compas
import geolocation.Position

data class Report(val position: Position, val orientation: Compas, val status: RoverStatus)