package mission

interface Monitor {
    fun display(rover: Rover, status: RoverStatus)
}