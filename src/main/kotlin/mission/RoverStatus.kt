package mission

enum class RoverStatus {
    HAVCO,
    WONTCO,
    CANTCO
}