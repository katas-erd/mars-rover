package mission

class ControlPanel(piloting: Rover) {
    val rover = piloting

    fun send(commands: String): RoverStatus {
        commands
            .map { it.toCommand() }
            .forEach { doThat ->
                val status = rover.doThat()
                if (status != RoverStatus.HAVCO)
                    return@send status
            }
        return RoverStatus.HAVCO
    }

    private fun Char.toCommand(): Rover.() -> RoverStatus {
        return when (this.uppercase()) {
            "F" -> Rover::`move forward`
            "B" -> Rover::`move backward`
            "L" -> Rover::`turn left`
            "R" -> Rover::`turn right`
            else -> {
                { RoverStatus.CANTCO }
            }
        }
    }
}